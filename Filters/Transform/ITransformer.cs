﻿using System.Drawing;

namespace MyPhotoshop
{
	public interface ITransformer<in TParameters>
		where TParameters : IParameters, new()
	{
		Size ResultSize { get; }

		void Prepare(Size size, TParameters parameters);
		Point? MapPoint(Point newPoint);
	}
}
