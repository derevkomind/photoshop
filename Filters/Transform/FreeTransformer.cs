﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;

namespace MyPhotoshop
{
	public class FreeTransformer : ITransformer<GrayscaleParameters>
	{
		private readonly Func<Size, Size> sizeTransformer;
		private readonly Func<Point, Size, Point> pointTransformer;
		private Size oldSize;

		public Size ResultSize { get; private set; }

		public FreeTransformer(Func<Size, Size> sizeTransformer, Func<Point, Size, Point> pointTransformer)
		{
			this.sizeTransformer = sizeTransformer;
			this.pointTransformer = pointTransformer;
		}
		
		public void Prepare(Size size, GrayscaleParameters parameters)
		{
			oldSize = size;
			ResultSize = sizeTransformer(oldSize);
		}

		public Point? MapPoint(Point newPoint)
		{
			return pointTransformer(newPoint, oldSize);
		}
	}
}
