﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPhotoshop
{
	public abstract class ParametrizedFilter<TParameters> : IFilter
		where TParameters : IParameters, new()
	{
		private readonly IParametersHandler<TParameters> handler = new ExpressionsParametersHandler<TParameters>();

		public string Name { get; }

		protected ParametrizedFilter(string name)
		{
			Name = name;
		}

		public ParameterInfo[] GetParameters()
		{
			return handler.GetDescription();
		}

		public Photo Process(Photo original, double[] values)
		{
			var parameters = handler.CreateParameters(values);
			return Process(original, parameters);
		}

		public abstract Photo Process(Photo original, TParameters parameters);

		public override string ToString() => Name;
	}
}
