﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyPhotoshop
{
	public struct Pixel
	{
		private double r;
		private double g;
		private double b;

		public Pixel(double r, double g, double b)
		{
			this.r = this.g = this.b = 0;
			R = r;
			G = g;
			B = b;
		}

		public double R
		{
			get => r;
			set => r = Check(value);
		}

		public double G
		{
			get => g;
			set => g = Check(value);
		}

		public double B
		{
			get => b;
			set => b = Check(value);
		}

		public double Check(double value)
		{
			if (value < 0 || value > 1)
				throw new ArgumentException();
			return value;
		}

		public static double Trim(double value)
		{
			if (value < 0) value = 0;
			if (value > 1) value = 1;

			return value;
		}

		public static Pixel operator *(Pixel pixel, double value)
		{
			return new Pixel(
				Trim(pixel.R * value),
				Trim(pixel.G * value),
				Trim(pixel.B * value));
		}

		public static Pixel operator *(double value, Pixel pixel)
		{
			return new Pixel(
				Trim(pixel.R * value),
				Trim(pixel.G * value),
				Trim(pixel.B * value));
		}
	}
}
