using System;

namespace MyPhotoshop
{
	public class Photo
	{
		private readonly Pixel[,] data;

		public int Width { get; }
		public int Height { get; }

		public ref Pixel this[int x, int y] => ref data[x, y];

		public Photo(int width, int height)
		{
			Width = width;
			Height = height;
			data = new Pixel[width, height];
		}
	}
}

